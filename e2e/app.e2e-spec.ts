import { NgLaravelPage } from './app.po';

describe('ng-laravel App', () => {
  let page: NgLaravelPage;

  beforeEach(() => {
    page = new NgLaravelPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
